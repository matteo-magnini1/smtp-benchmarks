package model;

public class Chronometer {

    private long startingTime = 0L;

    public void start() {
        this.startingTime = System.currentTimeMillis();
    }

    public long stop() {
        return System.currentTimeMillis() - startingTime;
    }
}
