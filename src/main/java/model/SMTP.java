package model;

import com.sun.mail.smtp.SMTPMessage;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

/**
 * SMTP client that directly communicates with the SMTP server.
 */
public class SMTP {

    private final Properties properties = System.getProperties();
    private final Chronometer chronometer = new Chronometer();
    private String username;
    private Optional<Session> session = Optional.empty();
    private Optional<Transport> transport = Optional.empty();


    public String getUsername() {
        return username;
    }

    /**
     * Start an authenticated connection connection with the SMTP server.
     *
     * @param ip SMTP server
     * @param port SMTP server
     * @param username of the user
     * @param password of the user
     * @return true if authentication has been successful, false otherwise.
     */
    public Boolean startConnection(String ip, String port, String username, String password){

        boolean status = true;
        this.username = username;
        this.properties.put("mail.smtp.host", username);
        this.properties.put("mail.smtp.port", port);
        this.session = Optional.of(Session.getInstance(properties, null));

        try {
            this.transport = Optional.of(session.get().getTransport("smtp"));
        } catch (NoSuchProviderException e) {
            status = false;
        }

        try {
            if (transport.isPresent()) {
                transport.get().connect(ip,username,password);
            }
        } catch (MessagingException e) {
            status = false;
        }
        return status;
    }

    /**
     * Shutdown the connection with the SMTP server.
     */
    public void stopConnection(){
        try {
            if (this.transport.isPresent()) {
                this.transport.get().close();
            }
        } catch (MessagingException e) {
            //Do nothing, connection already closed.
        }
    }

    /**
     * Send a mail throw the connection.
     *
     * @param to receiver
     * @param subject of the mail
     * @param text of the mail
     * @param files attached
     * @return time in millisecond, if -1 then the mail has not been delivered.
     */
    public long sendMessage(String to, String subject, String text, List<String> files){

        long time = -1L;
        if(this.session.isPresent()) {
            SMTPMessage msg = new SMTPMessage(this.session.get());
            try {
                msg.setFrom(new InternetAddress(this.username));
                msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));
                msg.setSubject(subject);
                msg.setHeader("X-Mailer", "msgsend");
                if(files.isEmpty()){
                    msg.setText(text);
                } else {
                    MimeBodyPart mbp1 = new MimeBodyPart();
                    mbp1.setText(text);
                    MimeMultipart mp = new MimeMultipart();
                    mp.addBodyPart(mbp1);
                    files.forEach(x -> {
                        try {
                            MimeBodyPart mbp2 = new MimeBodyPart();
                            mbp2.attachFile(x);
                            mp.addBodyPart(mbp2);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (MessagingException e) {
                            //Do nothing
                        }
                    });
                    msg.setContent(mp);
                }
                msg.setSentDate(new Date());
                msg.setReturnOption(SMTPMessage.RETURN_FULL);
                msg.setNotifyOptions(SMTPMessage.NOTIFY_SUCCESS);
                msg.saveChanges();
                if(this.transport.isPresent()) {
                    chronometer.start();
                    this.transport.get().sendMessage(msg, msg.getAllRecipients());
                    time = chronometer.stop();
                }
            } catch (MessagingException e) {
                System.out.println(e);
                time = -1L;
            }
        }
        return time;
    }
}
