package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/**
 * Keep track of the sent e-mails.
 */
public class Logbook {

    private final List<MailInfo> logbook = new ArrayList<>();

    public List<MailInfo> getLogbook() {
        return logbook;
    }

    public long getMaxDeliveryTime(List<MailInfo> logbook) {
        return logbook.stream()
                .max((x1,x2) -> (int)(x1.getDelivery()-x2.getDelivery()))
                .orElse(new MailInfo()).getDelivery();
    }

    public long getMinDeliveryTime(List<MailInfo> logbook) {
        return logbook.stream()
                .min((x1,x2) -> (int)(x1.getDelivery()-x2.getDelivery()))
                .orElse(new MailInfo()).getDelivery();
    }

    public long getAvgDeliveryTime(List<MailInfo> logbook) {
        return logbook.stream().map(MailInfo::getDelivery).reduce(Long::sum)
                .orElse(0L)/(this.logbook.size() > 0 ? this.logbook.size() : 1);
    }

    public void addMail(MailInfo mailInfo){
        this.logbook.add(mailInfo);
    }

    /**
     * Keep the mail information.
     */
    public static class MailInfo {
        private final long delivery;
        private final long size;
        private final Date date;
        private final String from;
        private final String to;

        public long getDelivery() {
            return delivery;
        }

        public long getSize() {
            return size;
        }

        public Date getDate() {
            return date;
        }

        public String getFrom() {
            return from;
        }

        public String getTo() {
            return to;
        }


        public MailInfo(long delivery, long size, Date date, String from, String to){
            this.delivery = delivery;
            this.size = size;
            this.date = date;
            this.from = from;
            this.to = to;
        }

        public MailInfo(){
            this.delivery = 0;
            this.size = 0;
            this.date = new Date();
            this.from = "";
            this.to = "";
        }
    }
}
