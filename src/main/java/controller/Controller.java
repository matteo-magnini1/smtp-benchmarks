package controller;

import model.Logbook;
import model.SMTP;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Controller {

    private final static String FILENAME = "data.csv";
    private final static long KB = 1024;
    private final SMTP smtp = new SMTP();
    private final Logbook logbook = new Logbook();
    private boolean isFilterActive = false;
    private int min = 0;
    private int max = 0;

    public Boolean startConnection(String ip, String port, String username, String password){
        return this.smtp.startConnection(ip,port,username,password);
    }

    public void stopConnection(){
        this.smtp.stopConnection();
    }

    public long sendMessage(String to, String subject, String text, List<String> files){
        long time = this.smtp.sendMessage(to,subject,text,files);
        if(time>=0) {
            long attachedFileSize = files.stream()
                    .map(x -> new File(x).length())
                    .reduce(Long::sum).orElse(0L);
            addMail(new Logbook.MailInfo(time, text.getBytes().length + attachedFileSize, new Date(), smtp.getUsername(), to));
        }
        return time;
    }

    public void activateFilter(String min, String max){
        this.isFilterActive = true;
        try {
            this.min = Integer.parseInt(min);
            this.max = Integer.parseInt(max);
        } catch (Exception e){
            //Do nothing;
        }
    }

    public void disableFilter(){
        this.isFilterActive = false;
    }

    public int getMailNumber() {
        return this.filter().size();
    }

    public long getMaxDeliveryTime() {
        return this.logbook.getMaxDeliveryTime(filter());
    }

    public long getMinDeliveryTime() {
        return this.logbook.getMinDeliveryTime(filter());
    }

    public long getAvgDeliveryTime() {
        return this.logbook.getAvgDeliveryTime(filter());
    }

    public void addMail(Logbook.MailInfo mailInfo){
        this.logbook.addMail(mailInfo);
    }

    public void save() throws FileNotFoundException {
        PrintWriter pw = new PrintWriter(new FileOutputStream(FILENAME));
        pw.println("date, from, to, size(B), delivery(ms)");
        for (Logbook.MailInfo mail : logbook.getLogbook())
            pw.println(mail.getDate() + ", "
                    + mail.getFrom() + ", "
                    + mail.getTo() + ", "
                    + mail.getSize() + ", "
                    + mail.getDelivery()
            );
        pw.close();
    }

    private List<Logbook.MailInfo> filter(){
        if(isFilterActive) {
            return this.logbook.getLogbook().stream()
                    .filter(x -> x.getSize() <= this.max * KB && x.getSize() >= this.min * KB)
                    .collect(Collectors.toList());
        } else {
            return this.logbook.getLogbook();
        }
    }
}
