package view;

import controller.Controller;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class Dashboard {
    public JPanel mainPanel;
    private JFormattedTextField to;
    private JFormattedTextField subject;
    private JTextArea mailBody;
    private JButton sendButton;
    private JFormattedTextField minSize;
    private JFormattedTextField maxSize;
    private JRadioButton filterRadioButton;
    private JButton exitButton;
    private JLabel mailNumber;
    private JLabel minTime;
    private JLabel maxTime;
    private JLabel avgTime;
    private JButton saveButton;
    private JButton clearButton;
    private JButton addFileButton;
    private JTextArea attachedFiles;

    private final Controller controller;
    private final List<String> files = new ArrayList<>();

    public Dashboard(Controller controller){

        this.controller = controller;
        this.mailBody.setLineWrap(true);
        this.mailBody.setWrapStyleWord(true);

        this.exitButton.addActionListener(e -> {
            this.controller.stopConnection();
            System.exit(0);
        });

        this.addFileButton.addActionListener(e -> {
            JFileChooser c = new JFileChooser();
            // Demonstrate "Open" dialog:
            int rVal = c.showOpenDialog(this.mainPanel);
            if (rVal == JFileChooser.APPROVE_OPTION) {
                this.files.add(c.getSelectedFile().getPath());
                this.attachedFiles.setText(getUpdatedList());
            }
        });

        this.sendButton.addActionListener(e -> {
            long time = controller.sendMessage(to.getText(),subject.getText(),mailBody.getText(),files);
            if(time>=0) {
                DeliveryAck ack = new DeliveryAck();
                ack.pack();
                ack.setLocationRelativeTo(null);
                ack.setVisible(true);
            } else {
                DeliveryError error = new DeliveryError();
                error.pack();
                error.setLocationRelativeTo(null);
                error.setVisible(true);
            }
            updateStatistics();
        });

        this.clearButton.addActionListener(e -> {
            this.files.clear();
            this.attachedFiles.setText("");
            this.mailBody.setText("");
            this.to.setText("");
            this.subject.setText("");
        });

        this.saveButton.addActionListener(e -> {
            try {
                this.controller.save();
            } catch (FileNotFoundException fileNotFoundException) {
                //Do nothing;
            }
        });

        this.filterRadioButton.addActionListener(e -> {
            if(this.filterRadioButton.isSelected()){
                this.controller.activateFilter(this.minSize.getText(), this.maxSize.getText());
            } else {
                this.controller.disableFilter();
            }
            this.updateStatistics();
        });

        this.minSize.getDocument().addDocumentListener(newUpdateDocumentListener());

        this.maxSize.getDocument().addDocumentListener(newUpdateDocumentListener());
    }

    private void updateStatistics(){
        mailNumber.setText(String.valueOf(controller.getMailNumber()));
        minTime.setText(String.valueOf(controller.getMinDeliveryTime()));
        maxTime.setText(String.valueOf(controller.getMaxDeliveryTime()));
        avgTime.setText(String.valueOf(controller.getAvgDeliveryTime()));
    }

    private void updateMinMax(){
        if(filterRadioButton.isSelected()){
            controller.activateFilter(minSize.getText(), maxSize.getText());
            updateStatistics();
        }
    }

    private String getUpdatedList(){
        return this.files.stream().map(x -> x + ";\n").reduce((x1,x2) -> x1+x2).orElse("");
    }

    private DocumentListener newUpdateDocumentListener(){
        return new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateMinMax();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateMinMax();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateMinMax();
            }
        };
    }
}
