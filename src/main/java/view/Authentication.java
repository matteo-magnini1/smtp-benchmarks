package view;

import controller.Controller;
import javax.swing.*;

/**
 * Authentication form for a SMTP server.
 * The user has to specify the server's ip and port along with his credentials.
 */
public class Authentication {
    public JPanel mainPanel;
    private JFormattedTextField serverIp;
    private JFormattedTextField serverPort;
    private JFormattedTextField username;
    private JPasswordField password;
    private JButton enterButton;

    public Authentication(Controller controller, View view){
        enterButton.addActionListener(e -> {
            String ip = serverIp.getText();
            String port = serverPort.getText();
            String name = username.getText();
            String pwd = new String(password.getPassword());
            Boolean status = controller.startConnection(ip,port,name,pwd);
            if (status) {
                view.startDashboard();
            } else {
                AuthenticationError error = new AuthenticationError();
                error.pack();
                error.setLocationRelativeTo(null);
                error.setVisible(true);
            }
        });
    }
}
