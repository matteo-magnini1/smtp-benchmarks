package view;

import controller.Controller;
import javax.swing.*;

/**
 * View logic for displaying windows and update them.
 */
public class View {

    private final Controller controller;
    private final JFrame authenticationFrame = new JFrame("SMTP authentication");
    private final JFrame dashboardFrame = new JFrame("SMTP dashboard");

    public View(Controller controller){
        this.controller = controller;
        Authentication authentication = new Authentication(controller, this);

        authenticationFrame.setContentPane(authentication.mainPanel);
        authenticationFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        authenticationFrame.pack();
        authenticationFrame.setLocationRelativeTo(null);
        authenticationFrame.setVisible(true);
    }

    public void startDashboard(){
        authenticationFrame.setVisible(false);
        Dashboard dashboard = new Dashboard(controller);
        dashboardFrame.setContentPane(dashboard.mainPanel);
        dashboardFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        dashboardFrame.pack();
        dashboardFrame.setLocationRelativeTo(null);
        dashboardFrame.setVisible(true);

    }
}
