package view;

import javax.swing.*;

public class DeliveryError extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;

    public DeliveryError() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        buttonOK.addActionListener(e -> onOK());
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    private void onOK() {
        this.setVisible(false);
        dispose();
    }
}
