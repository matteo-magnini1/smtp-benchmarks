package view;

import javax.swing.*;

public class DeliveryAck extends JDialog {
    private final static int TIMEOUT = 2000;
    private JPanel contentPane;
    private JButton buttonOK;

    public DeliveryAck() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        buttonOK.addActionListener(e -> onOK());
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        Timer timer = new Timer(TIMEOUT, e -> {
            setVisible(false);
            dispose();
        });
        timer.setRepeats(false);
        timer.start();
    }

    private void onOK() {
        setVisible(false);
        dispose();
    }
}