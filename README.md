# README #

# SMPT benchmark #

This simple application is used to perform same basic analysis over a SMTP service.

## Download ##

You can find a jar in the download.

## How to use ##

* Run the jar, you will see a form to connect to a reachable SMTP service.
* Once a connection is established you can send an e-mail to an address registered on the service (you can attach multiple files).
* You will have a visual feedback for the delivery and the statistics will be updated.
* If you want you can save the current data in a csv file for later analysis.